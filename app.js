const express = require("express");
const app = express();
const port = 8081;

app.get('/',(req,res)=>{
    res.send("This is my node app")
});
app.listen(port,()=>{
    console.log("App is running on port 8081")
});